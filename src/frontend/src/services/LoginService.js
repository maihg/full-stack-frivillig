import { Service } from "../services/Service.js";
import store from "../store/index.js";
import router from "../router/index.js";
import { createToast } from "mosha-vue-toastify";
const service = new Service();
const API_ENDPOINT = service.API_ENDPOINT;

export class LoginService {
    
    login(basic, email) {
        this.errors = [];
        service
            .postLogin(API_ENDPOINT + "/perform_login", basic)
            .then((data) => {
            if (data.status === 401) {
                if (this.errors.length === 0) {
                    console.log("wrong password or unvalid user")
                    this.errors.push("Wrong password or unvalid user");
                }

                createToast(
                    {
                        title: "Noe har gått galt",
                        description: "Se over brukernavn og passord og prøv igjen. " +
                            "Er du sikker på at brukeren din er gyldig?",
                    },
                    {
                        hideProgressBar: true,
                        swipeClose: true,
                        showIcon: true,
                        timeout: 10000,
                        position: "top-center",
                        type: "default",
                        transition: "slide",
                    });
            }else if (data.status === 200) {
                console.log("Hurra! Finne type")
                store.dispatch("login");

                //store userID and userName in store
                service.getData(API_ENDPOINT + "/user/email/" + email)
                .then(res =>  {
                    if(res.status === 200) {
                        res.json().then(data => {
                            console.log(data)
                            store.dispatch("setID", data.userID);
                            store.dispatch("setName", data.userFirstName);
                        })
                    }
                }).catch(err => console.log(err));


                //this part decides user type, saves it to store and routes the user/admin 
                //to the correct page. 
                service.getData(API_ENDPOINT + "/user/type/" + email)
                .then(function(body){
                return body.text(); // 
                }).then(function(data) {
                    //stores role of user (admin, user)
                    //store.dispatch("setRole", data);
                    if(data === "admin"){
                        store.dispatch("setRole", "admin");
                        router.push("http://localhost:3000/reservations");
                    }
                    else if(data === "user"){
                        store.dispatch("setRole", "user")
                        router.push('NewReservation');
                    }
                    });
            }
            })
            .catch((data) => {
            this.errors.push(
                "An error prevented login, Please contact us for more information"
            );
            console.log("ERROR:" , data);
            });
    }
  }
  