import { Service } from "../services/Service.js";
const service = new Service();
const API_ENDPOINT = service.API_ENDPOINT;

export class SectionService {
    /*
    * When user searches for available sections,
    * aka possible booking options based on user critera
    */
    async search(reservation){
        return service
        .postData(API_ENDPOINT + "/section/search", reservation)
        .then((data) => data.json())
        .then((res) => {
            return res; 
        });
    }
    
    reserve(sections){
        return service
        .postData(API_ENDPOINT + "/reservation", sections)
        .then((result) => {
            if(result.status === 200 || result.status === 202){
                return true; 
            }
            else {
                return false; 

            }
        })
    } 
}
  