import { Service } from "../services/Service.js";
import store from "@/store/index.js";
const service = new Service();
const API_ENDPOINT = service.API_ENDPOINT;

export class ReservationService {
    /*
    * When user searches for available sections,
    * aka possible booking options based on user critera
    */
    async getMyReservations(){
        return service
        .getData(API_ENDPOINT + `/reservation/${store.getters.getID}`)
            .then((data) => data.json())
            .then((res) => {
                return res; 
            });
    }

    removeReservation(reservationID){
        return service
        .delData(API_ENDPOINT + `/reservation/${reservationID}`)
            .then((data) => data.json())
            .then((res) => {
                return res; 
            });
    }
}