import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";

const store = createStore({
  state: {
    isAuthenticated: false,
    role: '',
    id : 0,
    name: ''
  },
  mutations: {
    SET_AUTHENTICATED(state, status) {
      console.log(state)
      console.log(status)
      state.isAuthenticated = status;
    },
    SET_ROLE(state, role){
      state.role = role;
    },
    SET_ID(state, id){
      state.id = id
    },
    SET_NAME(state, name) {
      state.name = name;
    }
  },
  actions: {
    login(context) {
      context.commit("SET_AUTHENTICATED", true);
    },
    logout(context) {
      context.commit("SET_AUTHENTICATED", false);
      context.commit("SET_ROLE", "");
    },
    setRole(context, role) {
      context.commit("SET_ROLE", role);
    },
    setID(context, id) {
      context.commit("SET_ID",id)
    },
    setName(context, name) {
      context.commit("SET_NAME", name)
    }
  },
  getters: {
    isAuthenticated(state) {
      return state.isAuthenticated;
    },
    getRole(state) {
      return state.role;
    },
    getID(state){
      return state.id; 
    },
    getName(state){
      return state.name;
    }
  },
  plugins: [
    createPersistedState({
      paths: ["isAuthenticated", "role", "id", "name"],
    }),
  ],
});

export default store
