import { createRouter, createWebHistory } from 'vue-router'
import store from "../store/index.js";
import Login from '../views/Login.vue'

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import('../views/admin/Admin.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/newUser',
    name: 'New User',
    component: () => import('../views/admin/NewUser.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/users',
    name: 'Users',
    component: () => import('../views/admin/Users.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/editUser/:id',
    name: 'Edit User',
    component: () => import('../views/admin/EditUser.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/reservations',
    name: 'Reservations',
    component: () => import('../views/admin/Reservations.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/editReservation/:id',
    name: 'Edit Reservation',
    component: () => import('../views/admin/EditReservation.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/newRoom',
    name: 'New Room',
    component: () => import('../views/admin/NewRoom.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/roomsAndSections',
    name: 'Rooms And Sections',
    component: () => import('../views/admin/RoomsAndSections.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/editRoom/:roomID',
    name: 'Edit Room',
    component: () => import('../views/admin/EditRoom.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/statistics',
    name: 'Statistics',
    component: () => import('../views/admin/Statistics.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: false }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta: { requiresAuth: true, adminAuth: true, userAuth: true }
  },
  {
    path: '/myProfile',
    name: 'MyProfile',
    component: () => import('../views/user/MyProfile.vue'),
    meta: { requiresAuth: true, adminAuth: false, userAuth: true }
  },
  {
    path: '/myReservations',
    name: 'MyReservations',
    component: () => import('../views/user/MyReservations.vue'),
    meta: { requiresAuth: true, adminAuth: false, userAuth: true }
  },
  {
    path: '/newReservation',
    name: 'NewReservation',
    component: () => import('../views/user/NewReservation.vue'),
    meta: { requiresAuth: true, adminAuth: false, userAuth: true }
  },
  {
    path: '/roomSchedule',
    name: 'RoomSchedule',
    component: () => import('../views/user/RoomSchedule.vue'),
    meta: { requiresAuth: true, adminAuth: false, userAuth: true }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters.isAuthenticated) {
      next({ name: "Login" });
    } else {
      if(to.meta.adminAuth) {
        console.log("I router index " + store.getters.getRole)
        if(store.getters.getRole === "admin") next();
        else next('/')
      } else if(to.meta.userAuth) {
        if(store.getters.getRole === "user") next();
        else next('/')
      }else{
        next();
      }
    }
  } else {
    next();
  }
});

export default router
