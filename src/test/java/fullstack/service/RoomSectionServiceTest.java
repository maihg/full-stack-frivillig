package fullstack.service;

import fullstack.model.Room;
import fullstack.model.Section;
import fullstack.repo.RoomSectionRepo;
import fullstack.service.RoomSectionService;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class RoomSectionServiceTest {
    @Mock
    private RoomSectionRepo repo;
    @InjectMocks
    private RoomSectionService service;

    @Test
    public void getAllRooms() {
        Room room1 = new Room(1, "Rom 123");
        Room room2 = new Room(45, "Rom 421");
        List<Room> rooms = Arrays.asList(room1, room2);
        Mockito.lenient().when(repo.getAllRooms()).thenReturn(rooms);

        List<Room> res = service.getAllRooms();
        assertEquals(rooms, res);
    }

    @Test
    public void findRoomByName() {
        Room room = new Room(1, "Rom 213");
        Mockito.lenient().when(repo.findRoomByName("Rom 213")).thenReturn(room);
        Mockito.lenient().when(repo.findRoomByName("")).thenReturn(null);

        Room res = service.findRoomByName("Rom 213");
        assertEquals(1, res.getRoomID());

        res = service.findRoomByName("");
        assertNull(res);
    }

    @Test
    public void addRoom() {
        Mockito.lenient().when(repo.addRoom(any(Room.class))).thenReturn(true);
        Room room = new Room();
        room.setRoomID(1);
        room.setRoomName("Det gule rommet");
        boolean result = service.addRoom(room);
        assertTrue(result);
    }

    @Test
    public void addSection() {
        Mockito.lenient().when(repo.addSection(any(Section.class))).thenReturn(true);
        Mockito.lenient().when(repo.findSection(any(Section.class)))
                .thenReturn(new Section(4, "Kroken", 4, 7));

        // Oppretter med sectionID=-2, det er en verdi vi aldri skal få tilbake fra metoden
        Section section = new Section(-2, "Kroken", 4, 7);
        int res = service.addSection(section);
        assertEquals(4, res);
    }

    /**
     * When addSection returns false, repo.findSection should not be run
     * If repo.findSection were run, res would be 5 here
     * If repo.findSection doesn't run, res will be -1
     */
    @Test
    public void addSectionFailed() {
        Mockito.lenient().when(repo.addSection(any(Section.class))).thenReturn(false);
        Mockito.lenient().when(repo.findSection(any(Section.class)))
                .thenReturn(new Section(5, "Kroken", 4, 7));

        Section section = new Section(-2, "Kroken", 4, 7);
        int res = service.addSection(section);
        assertEquals(-1, res);
    }

}