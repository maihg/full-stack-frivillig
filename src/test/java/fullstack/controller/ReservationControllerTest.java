package fullstack.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import fullstack.model.Reservation;
import fullstack.model.Room;
import fullstack.security.UserDetailsServiceProvider;
import fullstack.service.ReservationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Date;
import java.sql.Time;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReservationController.class)
class ReservationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReservationService service;
    @MockBean
    private UserDetailsServiceProvider provider;

    @Test
    @WithMockUser
    void makeReservations() throws Exception {
        when(service.makeReservations(any(ObjectNode.class))).thenReturn(true);

        this.mvc.perform(post("/api/reservation")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"date\": \"2021-12-12\", \"fromTime\": \"12:00:00\", \"toTime\": \"14:00:00\"," +
                        "\"comment\": \"Test\", \"userID\": 67, \"noPeople\": 0, \"sectionIDs\": [1, 43] }"))
                .andExpect(status().isOk());
    }

    @Test
    void getAllReservations() throws Exception {
        Reservation reservation = new Reservation("Test", new Date(System.currentTimeMillis()),
                new Time(1000), new Time(2000), 3, 0);
        List<Reservation> reservations =  Arrays.asList(reservation);

        when(service.getAllReservations()).thenReturn(reservations);

        this.mvc.perform(get("/api/reservations")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].comment").value("Test"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(1));
    }

    @Test
    @WithMockUser
    void findRoomForReservation() throws Exception {
        Room room = new Room(1, "Rom 213");

        when(service.findRoomForReservation(anyInt())).thenReturn(room);

        this.mvc.perform(get("/api/reservation/4/room")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.roomName").value("Rom 213"));
    }
}