package fullstack.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import fullstack.model.Equipment;
import fullstack.model.Room;
import fullstack.model.Section;
import fullstack.repo.RoomSectionRepo;
import fullstack.security.UserDetailsServiceProvider;
import fullstack.service.RoomSectionService;
import org.junit.Assert;
import org.junit.experimental.results.ResultMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RoomSectionController.class)
class RoomSectionControllerTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RoomSectionService service;
    @MockBean
    private UserDetailsServiceProvider provider;


    @Test
    @WithMockUser
    void getAlRooms() throws Exception {
        Room room1 = new Room(1, "Det gule rommet");
        Room room2 = new Room(2, "Utstyrsrom");
        List<Room> rooms = Arrays.asList(room1, room2);

        when(service.getAllRooms()).thenReturn(rooms);

        this.mvc.perform(get("/api/rooms")
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].roomName").value("Det gule rommet"));
    }

    @Test
    @WithMockUser
    void findRoomByName() throws Exception {
        Room room2 = new Room(2, "Utstyrsrom");
        when(service.findRoomByName("Utstyrsrom")).thenReturn(room2);

        this.mvc.perform(get("/api/room/Utstyrsrom")
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.roomName").value("Utstyrsrom"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roomID").value(2));
    }

    @Test
    @WithMockUser
    void addRoom() throws Exception {
        when(service.addRoom(any(Room.class))).thenReturn(true);

        this.mvc.perform(post("/api/room")
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"roomID\": -1, \"roomName\": \"Cowboyrommet\" }"))
                .andExpect(status().isCreated());

        verify(service).addRoom(any(Room.class));
    }

    @Test
    @WithMockUser
    void getAllSections() throws Exception {
        Section section1 = new Section(34, "Kroken", 1, 14);
        Section section2 = new Section(54, "Hele rommet", 72, 7);
        Section section3 = new Section(14, "Hjørnet ved isbjørnen", 1, 6);
        List<Section> sections =  Arrays.asList(section1, section2, section3);

        when(service.getAllSections()).thenReturn(sections);

        this.mvc.perform(get("/api/sections")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].sectionName").value("Kroken"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(3))
                .andDo(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    String a = JsonPath.parse(json).read("$[0].roomID").toString();
                    String b = JsonPath.parse(json).read("$[2].roomID").toString();
                    assertEquals(a, b);
                });
    }

    @Test
    @WithMockUser
    void addSection() throws Exception {
        Section section = new Section(-1, "Hele rommet", 4, 10);
        // addSection returner sectionID til ny seksjon hvis alt går bra, setter til å returnere 3
        when(service.addSection(any(Section.class))).thenReturn(3);

        MvcResult res = this.mvc.perform(post("/api/section")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"sectionID\": null, \"sectionName\": \"Hele rommet\", \"roomID\": 4, \"noOfSeats\": 10 }"))
                .andExpect(status().isCreated())
                .andReturn();

        // Sjekker om resultatet er lik id'en vi satte
        assertEquals("3", res.getResponse().getContentAsString());
    }

    @Test
    void getAllEquipment() throws Exception {
        Equipment equipment = new Equipment(5, "PC");
        List<Equipment> equipmentList = Collections.singletonList(equipment);

        when(service.getAllEquipment()).thenReturn(equipmentList);

        this.mvc.perform(get("/api/equipment")
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].equipmentDescription").value("PC"));
    }

    @Test
    @WithMockUser
    void addEquipment() throws Exception {
        when(service.addEquipment(anyString())).thenReturn(true);

        this.mvc.perform(post("/api/equipment")
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content("Lader til Mac"))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    void addEquipmentToSection() throws Exception {
        when(service.addEquipmentToSection("PC", 1, 2)).thenReturn(true);

        this.mvc.perform(post("/api/equipmentSection")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"equipmentDescription\": \"PC\", \"sectionID\": 1, \"numberOf\": 2 }"))
                .andExpect(status().isOk());
    }
}