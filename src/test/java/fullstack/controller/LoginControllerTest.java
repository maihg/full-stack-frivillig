package fullstack.controller;

import fullstack.security.UserDetailsImp;
import fullstack.security.UserDetailsServiceProvider;
import fullstack.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LoginController.class)
class LoginControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService service;
    @MockBean
    private UserDetailsServiceProvider provider;

    @Test
    @WithMockUser
    void signIn() throws Exception {
        when(service.userIsValid(any(UserDetailsImp.class))).thenReturn(true);

        this.mvc.perform(post("/api/perform_login")
                .accept(MediaType.APPLICATION_JSON)
                .content("{ \"username\": \"test\", \"password\": \"pass\" }"))
                .andExpect(status().isOk());
    }

}