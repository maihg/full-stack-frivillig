package fullstack.service;

import fullstack.model.User;
import fullstack.repo.*;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;

import fullstack.security.UserDetailsImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.stereotype.Service;




@Service 
public class UserService {
    Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private UserRepo repo;


    public UserService(){
    }

    /**
     * Register new user
     * @param user object with info (password=null)
     * @return true if the user is added to the db, false elsewise
     */
    public boolean registerUser(User user) {
        return repo.addUser(user);
    }

     /**
     * Get all users from the db
     * @return list of users
     */
    public List<User> getAllUsers() {
        return repo.getAllUsers();
    }

    /**
     * Find user from id
     * @param userID int
     * @return user object
     */
    public User findUserByID(int userID) {
        return this.repo.findUserByID(userID);
    }

    /**
     * Find user from id
     * @param email String
     * @return user object
     */
    public User findUserByEmail(String email) {
        return this.repo.findUserByEmail(email);
    }

     /**
     * Delete user by ID 
     * @param userID is ID of the user that is being deleted 
     * @return true if deleting works
     */
    public boolean deleteUser(int userID) {
        return repo.deleteUser(userID);
    }

    /**
     * Check if user is valid
     * @param email email
     * @param password password
     * @return
     */
    public boolean isValid(String email, String password){
        logger.info("user service recieved request to find user with email: " + email);
        return this.repo.userExists(email, password) && this.repo.userIsNotExpired(email);
    }

    /**
     * Checks if a user actually exist in the db
     * @param email the user email
     * @return true if the user exist, false elsewise
     */
    public boolean userExists(String email) {
        return repo.getUserByEmail(email) != null;
    }

    public boolean userIsValid(UserDetailsImp authenticationObj) {
        logger.info(authenticationObj.getUsername());
        return this.repo.userIsNotExpired(authenticationObj.getUsername());
    }


    public String getTypeByEmail(String email) {
        return repo.getUserTypeByEmail(email);
    }

    public int getIdByEmail(String email) {
        return repo.getIdByEmail(email); 
    }
}