package fullstack.service;


import com.fasterxml.jackson.databind.node.ObjectNode;
import fullstack.model.*;
import fullstack.model.Equipment;
import fullstack.repo.RoomSectionRepo;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



@Service
public class RoomSectionService {
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private RoomSectionRepo repo = new RoomSectionRepo();

    public List<Room> getAllRooms() {
        return this.repo.getAllRooms();
    }

    public Room findRoomByName(String roomName) { return this.repo.findRoomByName(roomName); }

    public boolean addRoom(Room room) {
        return this.repo.addRoom(room);
    }

    public boolean updateRoom(Room new_room) {
        return this.repo.updateRoom(new_room);
    }

    public boolean deleteRoom(int roomID) {
        return this.repo.deleteRoom(roomID);
    }

    public List<Section> getAllSections() {
        return this.repo.getAllSections();
    }

    public List<Section> getAllSectionsForRoom(int roomID) {
        return this.repo.getAllSectionsForRoom(roomID);
    }

    public int addSection(Section section) {
        if(this.repo.addSection(section)) {
            Section s = this.repo.findSection(section);
            if(s != null) {
                return s.getSectionID();
            }
        }
        return -1;
    }

    public Section findSection(Section section) { return this.repo.findSection(section); }

    public boolean updateSection(Section section) {
        return this.repo.updateSection(section);
    }

    public boolean deleteSection(int sectionID) {
        return this.repo.deleteSection(sectionID);
    }

    public List<Equipment> getAllEquipment() {
        return this.repo.getAllEquipment();
    }

    public boolean addEquipment(String equipmentDescription) {
        return this.repo.addEquipment(equipmentDescription);
    }

    public Equipment findEquipmentByDescription(String description) {
        return this.repo.findEquipmentByDescription(description);
    }

    public boolean addEquipmentToSection(String equipmentDescription, int sectionID, int numberOf) {
        Equipment e = this.repo.findEquipmentByDescription(equipmentDescription);
        if(e == null) {
            this.repo.addEquipment(equipmentDescription);
            e = this.repo.findEquipmentByDescription(equipmentDescription);
        }
        return this.repo.addEquipmentToSection(e.getEquipmentID(), sectionID, numberOf);
    }

    public List<Section> searchSections(ObjectNode  node) {
        logger.info("service recieved search request on available sections");
        List<Section> foundSections = new ArrayList<>(); 

        try{     
        SimpleDateFormat jsDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = sqlDateFormat.format(jsDateFormat.parse(node.get("date").asText()));
        
        String fraTidString = node.get("fromTime").asText(); 
        String tilTidString = node.get("toTime").asText();
        
        SimpleDateFormat timeConverterFormat = new SimpleDateFormat("HH:mm:ss");
        long fraTidConverted = timeConverterFormat.parse(fraTidString).getTime();
        long tilTidConverted = timeConverterFormat.parse(tilTidString).getTime(); 
        
        Date reservationDate = Date.valueOf(date); 
        Time fromTime = new Time(fraTidConverted);
        Time toTime = new Time(tilTidConverted);

        if(!node.get("roomname").asText().equals("none") && node.get("minNoSeats").asInt() != 0){
            foundSections = this.repo.searchAvailableSectionsWithRoomAndSeats(reservationDate, fromTime, toTime, node.get("roomname").asText(), node.get("minNoSeats").asInt());
            return foundSections; 
        }
        else if(!node.get("roomname").asText().equals("none")){
            foundSections = this.repo.searchAvailableSectionsWithRoom(reservationDate, fromTime, toTime, node.get("roomname").asText()); 
            return foundSections;
        }
        else if(node.get("minNoSeats").asInt() != 0){
            foundSections = this.repo.searchAvailableSectionsWithSeats(reservationDate, fromTime, toTime, node.get("minNoSeats").asInt()); 
            return foundSections;
        }
        else {
            foundSections = this.repo.searchAvailableSections(reservationDate, fromTime, toTime);
            return foundSections;
        }
        //filter på flere kriterier 
        } catch(Exception ex){
            ex.printStackTrace(); 
        }
        return foundSections; 
    }
}
