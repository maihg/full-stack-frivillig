package fullstack.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fullstack.model.Reservation;
import fullstack.model.Room;
import fullstack.model.Section;
import fullstack.repo.ReservationRepo;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private ReservationRepo repo = new ReservationRepo();

    public boolean makeReservations(ObjectNode node) {
        try {
            logger.info("service recieved message to make reservations");
            Reservation reservation = convertNodeToReservation(node);
            logger.info("res object made " + reservation.toString());
            //add reservation object, same for all sections that are to be reserved
            repo.addReservation(reservation);
            //find seservation ID for object made on the line over here
            int reservationID = repo.getReservationID(reservation);
            //iterate through all reservation objects, and
            //create reservation-section link in database
            int sectionId = 0;
            JsonNode array = node.get("sectionIDs");
            if(array.isArray()){
                for (JsonNode jsonNode : array){
                    sectionId = jsonNode.asInt();
                    repo.addReservationSection(reservationID, sectionId);
                }
            }
        } catch(Exception e){
            logger.error(e.toString()); 
            return false; 
        }
        return true; 
    }

    /**
     * Makes reservation object from object inside the node
     * @param node is node to fetch reservation info from
     * @return Reservation object
     */
    private Reservation convertNodeToReservation(ObjectNode node){
        logger.info("gonna make reservation object from node");

        try {
            SimpleDateFormat jsDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String date = sqlDateFormat.format(jsDateFormat.parse(node.get("date").asText()));

            String fraTidString = node.get("fromTime").asText();
            String tilTidString = node.get("toTime").asText();
            
            logger.info("tid format" + fraTidString);
            SimpleDateFormat timeConverterFormat = new SimpleDateFormat("HH:mm:ss");
            long fraTidConverted = timeConverterFormat.parse(fraTidString).getTime();
            long tilTidConverted = timeConverterFormat.parse(tilTidString).getTime();

            logger.info(date);
            Date reservationDate = Date.valueOf(date);
            Time fromTime = new Time(fraTidConverted);
            Time toTime = new Time(tilTidConverted);

            logger.info("converted to time and date, making object!!") ;
            return new Reservation(
                    node.get("comment").asText(),
                    reservationDate,
                    fromTime,
                    toTime,
                    node.get("userID").asInt(),
                    node.get("noPeople").asInt()
            );
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<Reservation> getAllReservations() {
        return this.repo.getAllReservations();
    }

    public Room findRoomForReservation(int reservationID) {
        return this.repo.findRoomForReservation(reservationID);
    }

    public List<Reservation> getAllReservationsForUser(int userID){
        return this.repo.getAllReservationsForUser(userID); 
    }

    public boolean removeReservation(int reservationID){
        boolean reservationRemoved = this.repo.removeReservationSection(reservationID); 
        boolean reservationSectionRemoved = this.repo.removeReservation(reservationID); 
        if(reservationSectionRemoved && reservationRemoved){
            return this.repo.removeReservation(reservationID);
        } 
        return false;       
    }

}
