package fullstack.repo;

import fullstack.model.Equipment;
import fullstack.model.Room;
import fullstack.model.Section;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger; 
import org.springframework.stereotype.Repository;



@Repository
public class RoomSectionRepo extends Repo {
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public RoomSectionRepo() {
        super();
    }

    /**
     * Method to get all rooms from the database
     * @return list of rooms
     */
    public List<Room> getAllRooms() {
        EntityManager em = emf.createEntityManager();
        List<Room> rooms = new ArrayList<>();
        try {
            Query q = em.createQuery("SELECT OBJECT(a) FROM room a", Room.class);
            q.getResultList().forEach(e -> {
                Room r = (Room) e;
                rooms.add(r);
            });
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception");
        } finally {
            em.close();
        }
        return rooms;
    }

    /**
     * Method to see if there exists a room by name
     * @param name roomName
     * @return null or a Room-object
     */
    public Room findRoomByName(String name) {
        EntityManager em = emf.createEntityManager();
        Room room = null;
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM room a WHERE a.roomName = ?1");
            q.setParameter(1, name);
            em.getTransaction().commit();
            room = (Room) q.getSingleResult();
            logger.info("Found a room with name " + name);
        }catch (Exception e) {
            // e.printStackTrace(); // Det er ok om den ikke finner noe
            logger.info("Didn't find a room with name " + name);
        }finally {
            em.close();
        }
        return room;
    }

    /**
     * Method to add a new room to the database
     * @param room the room to add
     * @return true if no exceptions acquired
     */
    public boolean addRoom(Room room) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("INSERT INTO room VALUES (NULL, ?1)");
            q.setParameter(1, room.getRoomName());
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Added room " + room.getRoomName() + " to database");
            return true;
        }catch (Exception e) {
            //e.printStackTrace();
            logger.error("Duplicate entry - cannot add room");
        }finally {
            em.close();
        }
        return false;
    }

    /**
     * Method that updates info on a room
     * @param new_room object with new info
     * @return true if all goes well (roomID that doesn't exist does not throw an exception)
     */
    public boolean updateRoom(Room new_room) {
        EntityManager em = emf.createEntityManager();
        try{
            em.getTransaction().begin();
            Query q = em.createNativeQuery("UPDATE room SET roomName=?1 WHERE room.roomID=?2");
            q.setParameter(1, new_room.getRoomName());
            q.setParameter(2, new_room.getRoomID());
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Updating name on room with id: " + new_room.getRoomID() + " , new name is: " + new_room.getRoomName());
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            em.close();
        }
        return false;
    }

    /**
     * Method that deletes a room from the database
     * @param roomID id for the room that should be deleted
     * @return true if all goes well
     */
    public boolean deleteRoom(int roomID) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("DELETE FROM room WHERE room.roomID=?1");
            q.setParameter(1, roomID);
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Deleted room with id: " + roomID);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            em.close();
        }
        return false;
    }

    /**
     * Method that finds all sections
     * @return list of sections
     */
    public List<Section> getAllSections() {
        EntityManager em = emf.createEntityManager();
        List<Section> sections = new ArrayList<>();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM section a");
            em.getTransaction().commit();
            q.getResultList().forEach(object -> {
                Section s = (Section) object;
                sections.add(s);
            });
        } catch (NoResultException nre) {
            // This is ok, unlikely, but ok
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        return sections;
    }

    /**
     * Method that finds all sections with roomID
     * @param roomID roomID to be looked for
     * @return list of sections
     */
    public List<Section> getAllSectionsForRoom(int roomID) {
        EntityManager em = emf.createEntityManager();
        List<Section> sections = new ArrayList<>();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM section a WHERE a.roomID = ?1");
            q.setParameter(1, roomID);
            em.getTransaction().commit();
            q.getResultList().forEach(object -> {
                Section s = (Section) object;
                sections.add(s);
            });
            logger.info("Antall seksjoner for rom " + roomID + ": " +sections.size());
            return sections;
        } catch (NoResultException nre) {
            // This might be ok, and might not be ok
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
        return sections;
    }

    /**
     * Method that adds a section to the database
     * @param section object with info to be added
     * @return true if all goes well
     */
    public boolean addSection(Section section) {
        EntityManager em = emf.createEntityManager();
        try{
            em.getTransaction().begin();
            Query q = em.createNativeQuery("INSERT INTO section VALUES (null, ?1, ?2, ?3)");
            q.setParameter(1, section.getSectionName());
            q.setParameter(2, section.getRoomID());
            q.setParameter(3, section.getNoOfSeats());
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Added section " + section.getSectionName() + " to database");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            em.close();
        }
        return false;
    }

    public Section findSection(Section section) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM section a WHERE a.roomID = ?1 AND a.sectionName = ?2");
            q.setParameter(1, section.getRoomID());
            q.setParameter(2, section.getSectionName());
            em.getTransaction().commit();
            Section s = (Section) q.getSingleResult();
            logger.info("Found section with id " + s.getSectionID());
            return s;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    /**
     * Method that updates a section in the database
     * @param new_section object with new info
     * @return true if all goes well
     */
    public boolean updateSection(Section new_section) {
        EntityManager em = emf.createEntityManager();
        try{
            em.getTransaction().begin();
            Query q = em.createNativeQuery("UPDATE section SET sectionName=?1, noOfSeats=?2 WHERE section.sectionID=?3");
            q.setParameter(1, new_section.getSectionName());
            q.setParameter(2, new_section.getNoOfSeats());
            q.setParameter(3, new_section.getSectionID());
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Updating section with id: " + new_section.getSectionID());
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            em.close();
        }
        return false;
    }

    /**
     * Method that deletes a section from the database
     * @param sectionID id of the section to be deleted
     * @return true if all goes well
     */
    public boolean deleteSection(int sectionID) {
        EntityManager em = emf.createEntityManager();
        try{
            em.getTransaction().begin();
            Query q = em.createNativeQuery("DELETE FROM section WHERE section.sectionID=?1");
            q.setParameter(1, sectionID);
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Deleted section with id: " + sectionID);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            em.close();
        }
        return false;
    }

    public List<Equipment> getAllEquipment() {
        EntityManager em = emf.createEntityManager();
        try {
            Query q = em.createQuery("SELECT OBJECT(a) FROM equipment a");
            return (List<Equipment>) q.getResultList();
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    public boolean addEquipment(String equipmentDescription) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("INSERT INTO equipment VALUES (null, ?1)");
            q.setParameter(1, equipmentDescription);
            q.executeUpdate();
            em.getTransaction().commit();
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            em.close();
        }
    }

    public Equipment findEquipmentByDescription(String description) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM equipment a WHERE a.equipmentDescription = ?1");
            q.setParameter(1, description);
            em.getTransaction().commit();
            Equipment equipment = (Equipment) q.getSingleResult();
            logger.info("Found quipment with name " + description);
            return equipment;
        }catch (Exception e) {
            //e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    public boolean addEquipmentToSection(int equipmentID, int sectionID, int numberOf) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("INSERT INTO equipmentSection VALUES (?1, ?2, ?3)");
            q.setParameter(1, equipmentID);
            q.setParameter(2, sectionID);
            q.setParameter(3, numberOf);
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Added equipment " + equipmentID + " to section " + sectionID);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            em.close();
        }
    }

    /**
     * Method that finds available sections
     * @param date on this date
     * @param fromTime from this time
     * @param toTime to this time
     * @return list of sections
     */
    public List<Section> searchAvailableSections(Date date, Time fromTime, Time toTime) {
        logger.info("finding sections with no preference");
        EntityManager em = emf.createEntityManager();
        List<Section> sections = new ArrayList<>();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("SELECT * FROM section WHERE section.sectionID in " + 
                "(SELECT reservationSection.sectionID FROM reservationSection WHERE reservationSection.reservationID NOT in " + 
                "(SELECT reservation.reservationID FROM reservation WHERE " +
                "reservation.Date= ?1 AND reservation.fromTime <= ?2 AND reservation.toTime >= ?3 )) " +
                "OR section.sectionID NOT IN (SELECT section.sectionID from section JOIN reservationSection on " + 
                "reservationSection.sectionID = section.sectionID)", Section.class);
        
            q.setParameter(1, date);
            q.setParameter(2, toTime);
            q.setParameter(3, fromTime);
            em.getTransaction().commit();
            List<Object[]> list = q.getResultList(); 
            for (Object section : list) {
                sections.add((Section) section);
            } 
        } catch (NoResultException nre) {
            // This might be ok, and might not be ok
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        return sections;
    }

    /**
     * Method that finds available sections
     * @param date on this date
     * @param fromTime from this time
     * @param toTime to this time
     * @param roomname name of room
     * @return list of sections
     */
    public List<Section> searchAvailableSectionsWithRoom(Date date, Time fromTime, Time toTime, String roomname) {
        logger.info("finding sections with room preference: " + roomname);
        EntityManager em = emf.createEntityManager();
        List<Section> sections = new ArrayList<>();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("SELECT * FROM section WHERE (section.sectionID in " + 
                "(SELECT reservationSection.sectionID FROM reservationSection WHERE reservationSection.reservationID NOT in " + 
                "(SELECT reservation.reservationID FROM reservation WHERE " +
                "reservation.Date= ?1 AND reservation.fromTime <= ?2 AND reservation.toTime >= ?3 )) " +
                "OR section.sectionID NOT IN (SELECT section.sectionID from section JOIN reservationSection on " + 
                "reservationSection.sectionID = section.sectionID)) " +
                "AND section.roomID = (SELECT room.roomID FROM room WHERE room.roomName = ?4)", Section.class);
            q.setParameter(1, date);
            q.setParameter(2, toTime);
            q.setParameter(3, fromTime);
            q.setParameter(4, roomname);
            em.getTransaction().commit();
            List<Object[]> list = q.getResultList(); 
            for (Object section : list) {
                sections.add((Section) section);
            } 
        } catch (NoResultException nre) {
            // This might be ok, and might not be ok
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        return sections;
    }

    /**
     * Method that finds available sections
     * @param date on this date
     * @param fromTime from this time
     * @param toTime to this time
     * @param minNoSeats sections must have at least this many seats
     * @return list of sections
     */
    public List<Section> searchAvailableSectionsWithSeats(Date date, Time fromTime, Time toTime, int minNoSeats) {
        logger.info("finding sections with seat preference: " + minNoSeats);
        EntityManager em = emf.createEntityManager();
        List<Section> sections = new ArrayList<>();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("SELECT * FROM section WHERE (section.sectionID in " + 
                "(SELECT reservationSection.sectionID FROM reservationSection WHERE reservationSection.reservationID NOT in " + 
                "(SELECT reservation.reservationID FROM reservation WHERE " +
                "reservation.Date= ?1 AND reservation.fromTime <= ?2 AND reservation.toTime >= ?3 )) " +
                "OR section.sectionID NOT IN (SELECT section.sectionID from section JOIN reservationSection on " + 
                "reservationSection.sectionID = section.sectionID))" +
                "AND section.noOfSeats >= ?4", Section.class);
            q.setParameter(1, date);
            q.setParameter(2, toTime);
            q.setParameter(3, fromTime);
            q.setParameter(4, minNoSeats);
            em.getTransaction().commit();
            List<Object[]> list = q.getResultList(); 
            for (Object section : list) {
                sections.add((Section) section);
            } 
        } catch (NoResultException nre) {
            // This might be ok, and might not be ok
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        return sections;
    }

    /**
     * Method that finds available sections
     * @param date on this date
     * @param fromTime from this time
     * @param toTime to this time
     * @param roomname name of room
     * @param minNoSeats must have at least this many seats
     * @return list of sections
     */
    public List<Section> searchAvailableSectionsWithRoomAndSeats(Date date, Time fromTime, Time toTime, String roomname, int minNoSeats) {
        logger.info("finding sections with room and seat preference: " + roomname + " " + minNoSeats);
        EntityManager em = emf.createEntityManager();
        List<Section> sections = new ArrayList<>();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("SELECT * FROM section WHERE ( section.sectionID in " + 
                "( SELECT reservationSection.sectionID FROM reservationSection WHERE reservationSection.reservationID NOT in " + 
                "( SELECT reservation.reservationID FROM reservation WHERE " +
                "reservation.Date = ?1 AND reservation.fromTime <= ?2 AND reservation.toTime >= ?3 ))" + 
                "OR section.sectionID NOT IN (SELECT section.sectionID from section JOIN reservationSection on " + 
                "reservationSection.sectionID = section.sectionID ))" +
                "AND section.noOfSeats >= ?4 AND " +
                "section.roomID = ( SELECT room.roomID FROM room WHERE room.roomName = ?5 )", Section.class);
            q.setParameter(1, date);
            q.setParameter(2, toTime);
            q.setParameter(3, fromTime);
            q.setParameter(4, minNoSeats);
            q.setParameter(5, roomname);
            em.getTransaction().commit();
            List<Object[]> list = q.getResultList(); 
            for (Object section : list) {
                sections.add((Section) section);
            } 
        } catch (NoResultException nre) {
            // This might be ok, and might not be ok
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        return sections;
    }
}
