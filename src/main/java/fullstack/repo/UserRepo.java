package fullstack.repo; 

import fullstack.model.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;  
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;






@Repository
public class UserRepo extends Repo {
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    //@Autowired
    //PasswordEncoder encoder; 
    @Autowired
    private JavaMailSender javaMailSender;

    public UserRepo() {
        super(); 
    }

    /**
     * Get all users from the database
     * @return list of users
     */
    public List<User> getAllUsers() { 
        EntityManager em = emf.createEntityManager();
        List<User> users = new ArrayList<>();
        try {
            Query q = em.createQuery("SELECT OBJECT(a) FROM user a");
            q.getResultList().forEach(e -> {
                User u = (User) e;
                users.add(u);
            }); 
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception");
        } finally {
            em.close();
        }
        return users;
    }

    /**
     * Method to find a user by id
     * @param userID the users id
     * @return user object
     */
    public User findUserByID(int userID) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM user a WHERE a.userID = ?1");
            q.setParameter(1, userID);
            em.getTransaction().commit();
            User user = (User) q.getSingleResult();
            logger.info("Found user with name " + user.getUserFirstName() + " " + user.getUserLastName());
            return user;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    /**
     * Method to find a user by id
     * @param email the users email
     * @return user object
     */
    public User findUserByEmail(String email) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM user a WHERE a.userEmail = ?1");
            q.setParameter(1, email);
            em.getTransaction().commit();
            User user = (User) q.getSingleResult();
            logger.info("Found user with name " + user.getUserFirstName() + " " + user.getUserLastName());
            return user;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    /**
     * Method that adds a user to the db
     * @param user object with info (with password=null)
     * @return true if all goes well
     */
    public boolean addUser(User user) {
        User newUser = new User(user.getUserFirstName(), user.getUserLastName(), user.getuserEmail(), user.getuserPhone(), user.getUserType(), user.getValidTo());
        EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(newUser);
            em.getTransaction().commit();
            logger.info(String.format("The user was added to the database", newUser.toString()));
            sendPasswordByEmail(newUser);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }


    /**
     * Method to send a user an email with a random generated string value that is saved in a hashmap that they can click
     * the link will reset the password to the given string value.
     * @param user the user which we send mail to with password. 
     */
    public void sendPasswordByEmail(User user){
        logger.info("attempting to send password by email");
        SimpleMailMessage msg = new  SimpleMailMessage();
        msg.setTo(user.getuserEmail());
        msg.setSubject("Velkommen til romreservasjon, " + user.getUserFirstName());
        msg.setText("Velkommen til vår romreservasjon-plattform\n\n Ditt førstegangs passord er: "+ user.getPassword() +"\n\nObs! Vi anbefaler deg å endre passord etter å ha logget inn for første gang. \n\nGod booking!");
        javaMailSender.send(msg);
        logger.info("Sent welcome email with password to: " + user.getuserEmail());
    }
    
    /**
     * Removes a user from the database
     * @param userID is the id of the user being deleted 
     * @return true if the user is deleted, false if an error occurs
     */
    public boolean deleteUser(int userID){
        logger.info("deleting user with ID " + userID); 
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("DELETE FROM user WHERE user.userID=?1");
            q.setParameter(1, userID);
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("User deleted with id " + userID);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public User getUserByEmail(String email){
        logger.info("repo recieved request to get user by email: " + email);
        EntityManager em = emf.createEntityManager();
        User a = null;
        try {
            Query q = em.createQuery("SELECT OBJECT(a) FROM user a WHERE a.userEmail=:email");
            q.setParameter("email", email);
            a = (User) q.getSingleResult();
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        if (a != null) {
            return a;
        } else {
            logger.warn("User was not found");
        }
        return null;
    }

    public boolean userExists(String email, String password){
        EntityManager em = emf.createEntityManager();    
        email = email.replace("%40", "@");
        logger.info("user repo recieved request to check user with email: " + email);
        User user = null;
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM user a WHERE a.userEmail=:email AND a.userPassword=:pwd");

            q.setParameter("email", email);
            q.setParameter("pwd", password);
            //q.executeUpdate();

            user = (User) q.getSingleResult();
            logger.info("result after sql: " + user);
            em.getTransaction().commit();
            
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            logger.warn("\nDISASTER\n");
            e.printStackTrace();
        } finally {
            em.close();
        }
        if (user != null) {
            logger.info("User was found");
            return true;
        } else {
            logger.info("User was not found");
            return false; 
        }
    }

    /**
     * Check if date is not past user validTo-date
     * @param email email of user
     * @return true if validTo-date is in future
     */
    public boolean userIsNotExpired(String email) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("SELECT validTo FROM user WHERE userEmail = ?1");
            q.setParameter(1, email);
            em.getTransaction().commit();
            Timestamp validTo = (Timestamp) q.getSingleResult();
            Date today = new Date();
            Timestamp now = new Timestamp(today.getTime());
            logger.info("Nå: " + now + ", validTo: " + validTo);
            return now.before(validTo);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            em.close();
        }
    }

	public String getUserTypeByEmail(String email) {
        logger.info("repo recieved request to get user type by email: " + email);
        EntityManager em = emf.createEntityManager();
        String type = "";
        try {
            Query q = em.createQuery("SELECT a.userType FROM user a WHERE a.userEmail=:email");
            q.setParameter("email", email);
            type = (String) q.getSingleResult();
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        if (type != "") {
            logger.info("found type:" + type);
            return type;
        } else {
            logger.warn("User was not found");
        }
        return null;
    }
    
    public int getIdByEmail(String email) {
        logger.info("repo recieved request to get id by email: " + email);
        EntityManager em = emf.createEntityManager();
        int id = -1;
        try {
            Query q = em.createQuery("SELECT a.userID FROM user a WHERE a.userEmail=:email");
            q.setParameter("email", email);
            id = (int) q.getSingleResult();
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
        if (id != -1) {
            logger.info("found type:" + id);
            return id;
        } else {
            logger.warn("User was not found");
        }
        return -1;
	}
}
