package fullstack.repo;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;

public class Repo {
    protected static EntityManagerFactory emf;

    public Repo() {
        try {
            connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Super class for all the repos that facilitates the connection to the mysql database
     * @throws IOException throws io exceptions
     */
    protected void connect() throws IOException {
        Properties prop = new Properties();
        HashMap<String, String> newProperties = new HashMap<>();
        //loads the local .properties file
        InputStream input = getClass().getClassLoader().getResourceAsStream("dbconfig.properties");
        // load a properties file
        prop.load(input);
        assert input != null;
        input.close();

        newProperties.put("javax.persistence.jdbc.url", "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/" + prop.getProperty("URL"));
        newProperties.put("javax.persistence.jdbc.user", prop.getProperty("USERNAME"));
        newProperties.put("javax.persistence.jdbc.password", prop.getProperty("PASSWORD"));

        emf = javax.persistence.Persistence.createEntityManagerFactory("km-db", newProperties);
    }

}

