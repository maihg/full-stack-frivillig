package fullstack.repo;

import fullstack.model.Reservation;
import fullstack.model.Room;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import fullstack.model.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;




@Repository
public class ReservationRepo extends Repo {
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public ReservationRepo() {
        super();
    }

    /**
     * Method to get all reservations from the database
     * @return list of reservations
     */
    public List<Reservation> getAllReservations() {
        EntityManager em = emf.createEntityManager();
        List<Reservation> reservations = new ArrayList<>();
        try {
            Query q = em.createQuery("SELECT OBJECT(a) FROM reservation a", Reservation.class);
            q.getResultList().forEach(e -> {
                Reservation r = (Reservation) e;
                reservations.add(r);
            });
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception");
        } finally {
            em.close();
        }
        return reservations;
    }

    /**
     * Method to add a new reservation to the database
     * @param reservation the reservation to add
     * @return true if no exceptions acquired
     */
    public boolean addReservation(Reservation reservation) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("INSERT INTO reservation VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)");
            q.setParameter(1, reservation.getReservationID());
            q.setParameter(2, reservation.getComment());
            q.setParameter(3, reservation.getDate());
            q.setParameter(4, reservation.getFromTime());
            q.setParameter(5, reservation.getToTime());
            q.setParameter(6, reservation.getUserID());
            q.setParameter(7, reservation.getNoPeople());
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Added reservation to database. Must add corresponding resercationSection"); 
        }catch (Exception e) {
            logger.error("Duplicate entry - cannot add");
            throw(e); 
        }finally {
            em.close();
        }
        return false;
    }

    public boolean addReservationSection(int reservationID, int sectionID){
        EntityManager em = emf.createEntityManager(); 
        try{
            em.getTransaction().begin(); 
            Query q = em.createNativeQuery("INSERT INTO reservationSection VALUES (?1, ?2)");
            q.setParameter(1, reservationID); 
            q.setParameter(2, sectionID);
            q.executeUpdate();
            em.getTransaction().commit();
        } catch(Exception e){
            logger.error("Duplicate entry - cannot add");
            throw(e); 
        } finally {
            em.close(); 
        }
        return false; 
    }

    public int getReservationID(Reservation reservation){
        int reservationID = -1; 
        EntityManager em = emf.createEntityManager(); 
        try{
            em.getTransaction().begin(); 
            Query q = em.createNativeQuery("SELECT r.reservationID FROM reservation r where r.Date = ?1 AND r.fromTime = ?2 AND r.toTime =?3 AND r.comment =?4"); 
            q.setParameter(1, reservation.getDate());
            q.setParameter(2, reservation.getFromTime()); 
            q.setParameter(3, reservation.getToTime()); 
            q.setParameter(4, reservation.getComment());
            em.getTransaction().commit();
            reservationID = (int) q.getSingleResult();
            logger.info("reservation ID found: " + reservationID);
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            em.close(); 
        }
        return reservationID; 
    }

    public List<Reservation> getAllReservationsForUser(int userID){
        EntityManager em = emf.createEntityManager();
        List<Reservation> reservations = new ArrayList<>();
        try {
            Query q = em.createQuery("SELECT OBJECT(a) FROM reservation a WHERE a.userID = ?1 ", Reservation.class);
            q.setParameter(1, userID);
            q.getResultList().forEach(e -> {
                Reservation r = (Reservation) e;
                reservations.add(r);
            });
        } catch (NoResultException nre) {
            //Ignore this because as per your logic this is ok!
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception");
        } finally {
            em.close();
        }
        return reservations;
    }

    public Room findRoomForReservation(int reservationID) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createQuery("SELECT OBJECT(a) FROM room a WHERE a.roomID IN " +
                    "(SELECT b.roomID FROM section b WHERE b.sectionID IN " +
                    "(SELECT c.sectionID FROM reservationSection c WHERE c.reservationID = ?1))" , Room.class);
            q.setParameter(1, reservationID);
            em.getTransaction().commit();
            Room room = (Room) q.getSingleResult();
            logger.info(room.toString());
            return room;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    public boolean removeReservation(int reservationID){
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("DELETE FROM reservation WHERE reservation.reservationID = ?1");
            q.setParameter(1, reservationID);
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Deleted reservation with id: " + reservationID);
            return true;
        } catch (Exception e) {
            logger.error("Caught exception while trying to delete reservation with id : " + reservationID);
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeReservationSection(int reservationID){
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("DELETE FROM reservationSection WHERE reservationSection.reservationID = ?1");
            q.setParameter(1, reservationID);
            q.executeUpdate();
            em.getTransaction().commit();
            logger.info("Deleted reservationSection with id: " + reservationID);
            return true;
        } catch (Exception e) {
            logger.error("Caught exception while trying to delete reservationSection with id : " + reservationID);
            e.printStackTrace();
            return false;
        }
    }
}


