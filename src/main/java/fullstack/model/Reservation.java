package fullstack.model;

import java.sql.Date;
import java.sql.Time;
import javax.persistence.*;

@Entity(name = "reservation")
@Table(name = "reservation")
@Cacheable(false)
public class Reservation {
    @Id
    @GeneratedValue
    @Column(name = "reservationID")
    private int reservationID;
    @Column(name = "comment")
    private String comment;
    @Column(name = "date")
    private Date date;
    @Column(name = "fromTime")
    private Time fromTime;
    @Column(name = "toTime")
    private Time toTime;
    @Column(name = "userID")
    private int userID;
    @Column(name = "noPeople")
    private int noPeople;

    public Reservation(String comment, Date date, Time fromTime, Time toTime, int userID, int noPeople) {
        this.comment = comment; 
        this.date = date; 
        this.fromTime = fromTime; 
        this.toTime = toTime; 
        this.userID = userID; 
        this.noPeople = noPeople;  
    }

    public Reservation() {

    }

    public int getReservationID(){
        return this.reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getFromTime() {
        return fromTime;
    }

    public void setFromTime(Time fromTime) {
        this.fromTime = fromTime;
    }

    public Time getToTime() {
        return toTime;
    }

    public void setToTime(Time toTime) {
        this.toTime = toTime;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int id) {
        this.userID = id;
    }


    public int getNoPeople() {
        return noPeople;
    }

    public void setNoPeople(int noPeople) {
        this.noPeople = noPeople;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "reservationID=" + reservationID +
                ", comment='" + comment + '\'' +
                ", date=" + date +
                ", fromTime=" + fromTime +
                ", toTime=" + toTime +
                ", userID=" + userID +
                ", noPeople=" + noPeople +
                '}';
    }
}

