package fullstack.model;

import javax.persistence.*;

@Entity(name = "equipment")
@Table(name = "equipment")
public class Equipment {
    @Id
    @GeneratedValue
    @Column(name = "equipmentID")
    private int equipmentID;
    @Column(name = "equipmentDescription")
    private String equipmentDescription;

    public Equipment() {
    }
    public Equipment(int equipmentID, String equipmentDescription) {
        this.equipmentID = equipmentID;
        this.equipmentDescription = equipmentDescription;
    }

    public int getEquipmentID() {
        return equipmentID;
    }

    public void setEquipmentID(int equipmentID) {
        this.equipmentID = equipmentID;
    }

    public String getEquipmentDescription() {
        return equipmentDescription;
    }

    public void setEquipmentDescription(String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }
}

