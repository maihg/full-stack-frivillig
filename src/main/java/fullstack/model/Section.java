package fullstack.model;

import javax.persistence.*;

@Entity(name = "section")
@Table(name = "section")
public class Section {
    @Id
    @GeneratedValue
    @Column(name = "sectionID")
    private int sectionID;
    @Column(name = "sectionName")
    private String sectionName;
    @Column(name = "roomID")
    private int roomID;
    @Column(name = "noOfSeats")
    private int noOfSeats;

    public Section() {}

    public Section(int sectionID, String sectionName, int roomID, int noOfSeats) {
        this.sectionID = sectionID;
        this.sectionName = sectionName;
        this.roomID = roomID;
        this.noOfSeats = noOfSeats;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }
}

