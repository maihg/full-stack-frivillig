package fullstack.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "reservationSection")
@Table(name = "reservationSection")
public class ReservationSection {
    @Id
    @Column(name = "reservationID")
    private int reservationID;
    @Id
    @Column(name = "sectionID")
    private int sectionID;

    public ReservationSection() {}

    public int getReservationID() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }
}

