package fullstack.model;

import javax.persistence.*;

@Entity(name = "room")
@Table(name = "room")
@Cacheable(false)
public class Room {
    @Id
    @GeneratedValue
    @Column(name = "roomID")
    private int roomID;
    @Column(name = "roomName")
    private String roomName;

    public Room(int roomID, String roomName) {
        this.roomID = roomID;
        this.roomName = roomName;
    }

    public Room() {

    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomID=" + roomID +
                ", roomName='" + roomName + '\'' +
                '}';
    }
}

