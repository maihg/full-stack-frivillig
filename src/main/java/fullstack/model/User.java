package fullstack.model;

import java.sql.Timestamp;
import java.util.Random;
import javax.persistence.*;

@Entity(name = "user")
@Table(name = "user")
@Cacheable(false)
public class User {
    @Id
    @GeneratedValue
    @Column(name = "userID")
    private int userID;

    @Column(name = "userFirstName")
    private String userFirstName;
    @Column(name = "userLastName")
    private String userLastName;
    @Column(name="userEmail", unique = true)
    private String userEmail;
    @Column(name="userPhone", unique = true)
    private int userPhone;
    @Column(name="userType")
    private String userType;
    @Column(name="validTo")
    private Timestamp validTo;
    @Column(name = "userPassword") //todo åssen fikse denne? 
    private String userPassword;
    
   
    public User(){
    }

    public User(String userFirstName, String userLastName, String userEmail, int userPhone, String userType, Timestamp validTo) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName; 
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.userType = userType; 
        this.validTo = validTo; 
        setGeneratedPassword(); 
    }

    public void setUserID(int userID){
        this.userID = userID; 
    }
    public int getUserID(){
        return this.userID; 
    }

    public void setUserFirstName(String name){
        this.userFirstName = name; 
    }
    public String getUserFirstName(){
        return this.userFirstName; 
    }

    public void setUserLastName(String name){
        this.userLastName = name; 
    }

    public String getUserLastName(){
        return this.userLastName; 
    }

    public void setuserEmail(String userEmail){
        this.userEmail = userEmail; 
    }
    public String getuserEmail(){
        return this.userEmail; 
    }

    public void setuserPhone(int userPhone){
        this.userPhone = userPhone; 
    }
    public int getuserPhone(){
        return this.userPhone; 
    }

    public void setUserType(String type){
        this.userType = type; 
    }
    public String getUserType(){
        return this.userType; 
    }

    public void setValidTo(Timestamp validTo){
        this.validTo = validTo; 
    }
    public Timestamp getValidTo(){
        return this.validTo;
    }

    public String getPassword() {
        return this.userPassword;
    }
  
    public void setPassword(String password) {
        this.userPassword = password;
    }

    public void setGeneratedPassword() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 12;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) 
                (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        this.userPassword = buffer.toString();  
    }


    @Override
    public String toString() {
        return "User{" +
                "userID=" + this.userID +
                ", name='" + this.userFirstName + " " + this.userLastName + '\'' +
                ", email='" + this.userEmail + '\'' +
                ", phone='" + this.userPhone + '\'' +
                ", type='" + this.userType + '\'' +
                ", valid to='" + this.validTo + '\'' +
                ", password'" + this.userPassword + '\'' +
                '}';
    }
}

