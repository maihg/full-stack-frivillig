package fullstack.model;

import javax.persistence.*;

@Entity(name = "equipmentSection")
@Table(name = "equipmentSection")
public class EquipmentSection {
    @Id
    @Column(name = "equipmentID")
    private int equipmentID;
    @Id
    @Column(name = "sectionID")
    private int sectionID;
    @Column(name = "numberOf")
    private int numberOf;

    public EquipmentSection() {
    }

    public EquipmentSection(int equipmentID, int sectionID, int numberOf) {
        this.equipmentID = equipmentID;
        this.sectionID = sectionID;
        this.numberOf = numberOf;
    }

    public int getEquipmentID() {
        return equipmentID;
    }

    public void setEquipmentID(int equipmentID) {
        this.equipmentID = equipmentID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public int getNumberOf() {
        return numberOf;
    }

    public void setNumberOf(int numberOf) {
        this.numberOf = numberOf;
    }
}

