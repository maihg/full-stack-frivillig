package fullstack.controller;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;
import fullstack.model.Equipment;
import fullstack.model.Room;
import fullstack.model.Section;
import fullstack.model.Reservation;
import fullstack.service.RoomSectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping("/api")
public class RoomSectionController {
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private RoomSectionService roomSectionService;

    @GetMapping("/rooms")
    @ResponseStatus(HttpStatus.OK)
    public List<Room> getAllRooms() {
        return this.roomSectionService.getAllRooms();
    }

    @GetMapping("/room/{roomName}")
    @ResponseStatus(HttpStatus.OK)
    public Room findRoomByName(@PathVariable String roomName) {
        Room res = this.roomSectionService.findRoomByName(roomName);
        if(res != null) return res;
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/room")
    @ResponseStatus(HttpStatus.CREATED)
    public boolean addRoom(@RequestBody Room room) {
        boolean res = this.roomSectionService.addRoom(room);
        if(res) return true;
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/room")
    @ResponseStatus(HttpStatus.OK)
    public boolean updateRoom(@RequestBody Room new_room) {
        return this.roomSectionService.updateRoom(new_room);
    }

    @DeleteMapping("/room/{roomID}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteRoom(@PathVariable int roomID) {
        return this.roomSectionService.deleteRoom(roomID);
    }

    @GetMapping("/sections")
    @ResponseStatus(HttpStatus.OK)
    public List<Section> getAllSections() {
        return this.roomSectionService.getAllSections();
    }

    @GetMapping("{roomID}/sections")
    @ResponseStatus(HttpStatus.OK)
    public List<Section> getAllSectionsForRoom(@PathVariable int roomID) {
        return this.roomSectionService.getAllSectionsForRoom(roomID);
    }

    @PostMapping("/section")
    @ResponseStatus(HttpStatus.CREATED)
    public int addSection(@RequestBody Section section) {
        return this.roomSectionService.addSection(section);
    }

    @GetMapping("/section")
    @ResponseStatus(HttpStatus.OK)
    public Section findSection(@RequestBody Section section) {
        Section s = this.roomSectionService.findSection(section);
        if(s != null) {
            return s;
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/section")
    @ResponseStatus(HttpStatus.OK)
    public boolean updateSection(@RequestBody Section section) {
        return this.roomSectionService.updateSection(section);
    }

    @DeleteMapping("/section/{sectionID}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteSection(@PathVariable int sectionID) {
        return this.roomSectionService.deleteSection(sectionID);
    }

    @GetMapping("/equipment")
    @ResponseStatus(HttpStatus.OK)
    public List<Equipment> getAllEquipment() {
        return this.roomSectionService.getAllEquipment();
    }

    @PostMapping("/equipment")
    @ResponseStatus(HttpStatus.CREATED)
    public boolean addEquipment(@RequestBody String equipmentDescription) {
        if(this.roomSectionService.addEquipment(equipmentDescription)) {
            return true;
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/equipment/{equipmentDescription}")
    @ResponseStatus(HttpStatus.OK)
    public Equipment findEquipmentByDescription(@PathVariable String equipmentDescription) {
        return this.roomSectionService.findEquipmentByDescription(equipmentDescription);
    }

    @PostMapping("/equipmentSection")
    @ResponseStatus(HttpStatus.OK)
    public boolean addEquipmentToSection(@RequestBody ObjectNode node) {
        return this.roomSectionService.addEquipmentToSection(
                node.get("equipmentDescription").asText(),
                node.get("sectionID").asInt(),
                node.get("numberOf").asInt());

    }

    @PostMapping("/section/search")
    public List<Section> searchSections(@RequestBody ObjectNode reservation) {
        logger.info("Controller recieved search request on available sections");
        return this.roomSectionService.searchSections(reservation);
    }
}


