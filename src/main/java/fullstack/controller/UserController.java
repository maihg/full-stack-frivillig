package fullstack.controller;

import fullstack.model.User;
import fullstack.service.UserService;
import java.sql.Timestamp;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;



@RestController
@RequestMapping("/api")
public class UserController {
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    @Autowired
    private UserService userService;

    public UserController(){
    }


      /**
     * Endpoint to register a user
     * @param user is the user who is being registered
     * @return 201 created if the user is created or 409 conflict if the user allready exist.
     */
    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public boolean register(@RequestBody User user) { //@Requestbody
        logger.info("recieved request to register user");
        return userService.registerUser(user); //send auto email med passord
    }

    /**
     * Endpoint to find a user by id
     * @param userID int id
     * @return 200 if user found, 404 if user not found
     */
    @GetMapping("/user/{userID}")
    @ResponseStatus(HttpStatus.OK)
    public User findUserByID(@PathVariable int userID) {
        User user = this.userService.findUserByID(userID);
        if(user != null) return user;
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/user/email/{email}")
    @ResponseStatus(HttpStatus.OK)
    public User findUserByEmail(@PathVariable String email) {
        User user = this.userService.findUserByEmail(email);
        if(user != null) return user;
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

      /**
     * Endpoint to fetch all users
     * @return list of registered users
     */
    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        logger.info("recieved request to get all users");
        return userService.getAllUsers(); 
    }


    /**
     * Endpoint to register a user
     * @param userID is the user who is being registered
     * @return 201 created if the user is created or 409 conflict if the user allready exist.
     */
    @DeleteMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteUser(@RequestBody int userID) {
        logger.info("recieved request to delete user " + userID);
        return userService.deleteUser(userID); //send auto email med passord
    }



    /**
     * Ednpoint to check if an email exists
     * @param email the email to be checked
     * @return true or 404.
     */
    @GetMapping("/user/exists/{email}")
    @ResponseStatus(HttpStatus.OK)
    public boolean checkEmail(@PathVariable("email") String email) {
        if (email != null)
            return userService.userExists(email);
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }


    /**
     * Endpoint to get user type
     * @param email is the user who is being registered
     * @return 201 created if the user is created or 409 conflict if the user allready exist.
     */
    @GetMapping("/user/type/{email}")
    @ResponseStatus(HttpStatus.OK)
    public String getTypeByEmail(@PathVariable("email") String email) {
        logger.info("recieved request to find type of user with email " + email);
        return userService.getTypeByEmail(email); //send auto email med passord
    }

    @GetMapping("/user/id/{email}")
    @ResponseStatus(HttpStatus.OK)
    public int getIdByEmail(@PathVariable("email") String email) {
        logger.info("recieved request to find id of user with email " + email);
        return userService.getIdByEmail(email); 
    }

}