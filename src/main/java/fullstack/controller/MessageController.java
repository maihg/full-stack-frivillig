package fullstack.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/messages")
public class MessageController {

    @GetMapping("/hello")
    public String hello() {
        return "Full Stack Java with Spring Boot and VueJS!";
    }

    @GetMapping("/allMessages")
    public ArrayList<String> getAllMessages() {
        ArrayList<String> messages = new ArrayList<>();
        messages.add("Test 1");
        messages.add("Reservert rom 1, seksjon A, 12.03.21 13:00 til 12.03.21 15:00");

        return messages;
    }
}
