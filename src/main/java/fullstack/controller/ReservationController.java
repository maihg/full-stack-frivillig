package fullstack.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import fullstack.model.Reservation;
import fullstack.model.Room;
import fullstack.service.ReservationService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping("/api")
public class ReservationController {
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private ReservationService reservationService;

    @PostMapping("/reservation")
    public Boolean makeReservations(@RequestBody ObjectNode reservations) {
        logger.info("Controller recieved request to create reservations");
        return this.reservationService.makeReservations(reservations);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/reservation/{userID}")
    public List<Reservation> getAllReservationsForUser(@PathVariable("userID") int userId){
        logger.info("Controller recieved request to find reservations for user " + userId);
        return this.reservationService.getAllReservationsForUser(userId); 

    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/reservation/{reservationID}")
    public boolean removeReservation(@PathVariable("reservationID") int reservationID){
        logger.info("Controller recieved request to remove reservation with id " + reservationID);
        return this.reservationService.removeReservation(reservationID); 
    }

    @GetMapping("/reservations")
    @ResponseStatus(HttpStatus.OK)
    public List<Reservation> getAllReservations() {
        return this.reservationService.getAllReservations();
    }

    @GetMapping("/reservation/{reservationID}/room")
    @ResponseStatus(HttpStatus.OK)
    public Room findRoomForReservation(@PathVariable int reservationID) {
        Room room = this.reservationService.findRoomForReservation(reservationID);
        if(room != null) return room;
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

}


