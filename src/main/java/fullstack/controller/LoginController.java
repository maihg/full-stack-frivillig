package fullstack.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fullstack.model.User;
import fullstack.security.UserDetailsImp;
import fullstack.service.UserService;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class LoginController {
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    UserService userService; 

    public LoginController() {
    }

      /**
     * Endpoint which allows a user to log in.
     * @param authentication The user who is logging in
     * @return The details of the user who just logged in.
     */
    @PostMapping("/perform_login")
    @ResponseStatus(HttpStatus.OK)
    public UserDetailsImp signIn(Authentication authentication) {
        logger.info("user logged in: "+ authentication.getCredentials());

        try {
            UserDetailsImp imp = (UserDetailsImp) authentication.getPrincipal();
            if(!this.userService.userIsValid(imp)) {
                logger.info("-- not valid anymore");
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            return imp;
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e){
            return (UserDetailsImp) authentication.getDetails();
        }
    }

    /**
     * Endpoint to login
     * @param userLogin is containing login-attempt-info
     * @return object with token if user is found! 
     */
     @ResponseBody
     @PostMapping("/login")
     public ResponseEntity<Object> login(@RequestBody User userLogin) {
         logger.info("request til login mottatt");
         if (userService.isValid(userLogin.getuserEmail(), userLogin.getPassword())) {
            String token = "ok"; 
             //String token = "Bearer "+ securityService.createToken(username, 10*100);
             MultiValueMap<String, String> headers = new HttpHeaders(); //HttpHeaders headers = new HttpHeaders();
             headers.add("Authorization", token); // JWT should be in the header of the HTTP response
             headers.add("Access-Control-Expose-Headers", "Authorization");
 
             logger.info("returnerer response-entity");
             return new ResponseEntity<Object>(headers, HttpStatus.OK);
         } else
         {
             throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not a valid username/password combo.");
         }
     }
 

    /**
     * Endpoint to log out a user
     * 
     * @param authentication the authentication of the curently loggen in user
     * @return true if the user is logged out, false if not.
     */
    @PostMapping("/performLogout")
    public boolean logout(Authentication authentication) {
        authentication.setAuthenticated(false);
        logger.info("User logged out ");
        return true;
    }
}