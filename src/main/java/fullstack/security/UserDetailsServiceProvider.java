package fullstack.security; 

import org.springframework.stereotype.Service;

import fullstack.model.User;
import fullstack.repo.UserRepo; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Service
public class UserDetailsServiceProvider implements UserDetailsService {

    @Autowired
    UserRepo userRepo; 

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepo.getUserByEmail(email);
        if (user == null) throw new UsernameNotFoundException(email + " not found");
        return new UserDetailsImp(user);
    }
}