package fullstack.security;

import java.util.Arrays;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;



@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    @Autowired
    UserDetailsServiceProvider serviceprovider; 

    public SecurityConfig(){
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
                .csrf()
                .disable()
                .cors()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/user").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user/{userID}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user/id/{email}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user/email/{email}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user/exists/{email}").permitAll()
                .antMatchers(HttpMethod.POST, "/api/perform_login").permitAll()
                .antMatchers(HttpMethod.POST, "/api/login").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user/type/{email}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/section/search").permitAll()
                .antMatchers(HttpMethod.POST, "/api/section/search").permitAll()
                .antMatchers(HttpMethod.GET, "/api/sections").permitAll()
                .antMatchers(HttpMethod.POST, "/api/reservation").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/room").permitAll()
                .antMatchers(HttpMethod.GET, "/api/room").permitAll()
                .antMatchers(HttpMethod.GET, "/api/reservation/{reservationID}/room").permitAll()
                .antMatchers(HttpMethod.GET, "/api/rooms").permitAll()
                .antMatchers(HttpMethod.GET, "/api/{roomID}/sections").permitAll()
                .antMatchers(HttpMethod.GET, "/api/room/{roomName}").permitAll()
                .antMatchers(HttpMethod.POST, "/api/room").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/room").permitAll()
                .antMatchers(HttpMethod.POST, "/api/section").permitAll()
                .antMatchers(HttpMethod.GET, "/api/section").permitAll()
                .antMatchers(HttpMethod.GET, "/api/equipment").permitAll()
                .antMatchers(HttpMethod.GET, "/api/reservation/{userID}").permitAll()
                .antMatchers(HttpMethod.POST, "/api/equipment").permitAll()
                .antMatchers(HttpMethod.GET, "/api/equipment/{equipmentDescription}").permitAll()
                .antMatchers(HttpMethod.POST, "/api/equipmentSection").permitAll()
                .antMatchers(HttpMethod.GET, "/api/reservations").permitAll()
                .antMatchers(HttpMethod.GET, "/api/reservation/{reservationID}/room").permitAll()
                .antMatchers("/", "/login**","/js/**","/callback/", "/webjars/**", "/error**", "/css/**", "/resources/**").permitAll()
                .antMatchers("/admin").access("hasRole('admin')")
                .antMatchers("/user").access("hasRole('user')")

                .anyRequest()
                .authenticated()
                .and()
                    .formLogin()
                    .loginPage("/")
                    .loginProcessingUrl("/perform_login")
                    //.defaultSuccessUrl("/user.html", true)
                    .failureUrl("/")
                    //.failureHandler(authenticationFailureHandler())
                .and()
                .httpBasic().authenticationEntryPoint(new NoPopupBasicAuthenticationEntryPoint());
    }

    

    //noOp because no hashing yet 
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();  //klartekst, ops 
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:8080", "http://localhost:3000"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Collections.singletonList("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

     /**
     * This function encodes and decodes the passwords and checks if the passwords matches
     * @return wheter a user is authenticated or not.
     */
    @Bean
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(getPasswordEncoder());
        provider.setUserDetailsService(this.serviceprovider);
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(serviceprovider); 
    }
    
}
