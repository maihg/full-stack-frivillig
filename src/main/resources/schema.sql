DROP TABLE IF EXISTS reservationSection;
DROP TABLE IF EXISTS equipmentSection;
DROP TABLE IF EXISTS section;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS equipment;
DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS user;


CREATE TABLE room (
    roomID INT AUTO_INCREMENT  PRIMARY KEY,
    roomName VARCHAR(250) NOT NULL UNIQUE
);

CREATE TABLE section (
    sectionID INT AUTO_INCREMENT  PRIMARY KEY,
    sectionName VARCHAR(50) NOT NULL,
    roomID INT NOT NULL,
    noOfSeats INT NOT NULL,
    FOREIGN KEY(roomID) REFERENCES room(roomID)
);
ALTER TABLE section
    ADD UNIQUE KEY roomID_sectionName(roomID, sectionName);

CREATE TABLE equipment (
    equipmentID INT AUTO_INCREMENT  PRIMARY KEY,
    equipmentDescription VARCHAR(100) NOT NULL
);

CREATE TABLE user (
    userID INT AUTO_INCREMENT  PRIMARY KEY,
    userFirstName VARCHAR(50) NOT NULL,
    userLastName VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    phone int NOT NULL,
    userType VARCHAR(50) NOT NULL,
    validTo DATETIME NOT NULL,
    userPassword VARCHAR(50) NOT NULL
);

CREATE TABLE reservation (
    reservationID INT AUTO_INCREMENT PRIMARY KEY,
    comment VARCHAR(50) NOT NULL,
    Date DATE NOT NULL,
    fromTime TIME NOT NULL,
    toTime TIME NOT NULL,
    userID INT NOT NULL,
    noPeople INT,
    FOREIGN KEY(userID) REFERENCES user(userID)
);

CREATE TABLE reservationSection (
    reservationID INT,
    sectionID INT,
    PRIMARY KEY(reservationID, sectionID),
    FOREIGN KEY(reservationID) REFERENCES reservation(reservationID),
    FOREIGN KEY(sectionID) REFERENCES section(sectionID)
);


CREATE TABLE equipmentSection (
    equipmentID INT,
    sectionID INT,
    numberOf INT NOT NULL,
    PRIMARY KEY (equipmentID, sectionID),
    FOREIGN KEY(equipmentID) REFERENCES equipment(equipmentID),
    FOREIGN KEY(sectionID) REFERENCES section(sectionID)
);
